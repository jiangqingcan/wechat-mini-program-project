// pages/login/index.js
import{
loginRequest
} from "../../api/main"
Page({

  /**
   * 页面的初始数据
   */
  data: {
     stuId:'admin',            //账号密码
     password:'1234'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
  },
  //登录
login(){
  const that = this
  const postData = {              //传递body参数 账号密码
       stuId:that.data.stuId,
       password:that.data.password
  }
  wx.showLoading({
    title: '登录中',
  })
loginRequest(postData).then(res =>{
  wx.hideLoading()
  if(res.code == -1){
    wx.showToast({
      title: res.msg,
      icon:'none'
    })
    return
  }
  wx.setStorageSync('token', res.data.cookie)
  wx.showToast({
    title: '登录成功',
    icon:'success'
  })
  setTimeout(() =>{
    wx.redirectTo({
      url: '/pages/index/index',
    })
  },3000);
})
 
}
 
})